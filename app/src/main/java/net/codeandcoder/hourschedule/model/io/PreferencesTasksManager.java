package net.codeandcoder.hourschedule.model.io;

import android.content.SharedPreferences;

import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.model.calendar.Year;
import net.codeandcoder.hourschedule.model.utils.JSONStructureParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class PreferencesTasksManager implements TasksManager {

    private static final String TASKS_KEY = "tasks";
    private SharedPreferences prefs;

    public PreferencesTasksManager(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    @Override
    public List<ScheduleTask> readData() {
        List<ScheduleTask> tasks = null;

        String json = prefs.getString(TASKS_KEY, null);

        // If there's no data stored, it generates test tasks
        if (json == null) {
            tasks = ScheduleTask.generateTestTasks();
        } else {
            try {
                JSONObject jsonObject = new JSONObject(json);
                tasks = JSONStructureParser.parseTasks(jsonObject);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return tasks;
    }

    @Override
    public void saveData(List<ScheduleTask> tasks) {
        JSONObject jsonToSave = JSONStructureParser.formatTasks(tasks);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(TASKS_KEY, jsonToSave.toString());
        prefsEditor.commit();
    }
}
