package net.codeandcoder.hourschedule.model.calendar;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class Week {

    private List<Day> days;

    public Week(List<Day> days) {
        this.days = days;
    }

    public List<Day> getDays() {
        return days;
    }

    public Day getDay(int index) {
        return days.get(index);
    }

    public Day getDayByDate(long date) {
        for (Day d : days) {
            if (d.getDate() == date) {
                return d;
            }
        }

        return null;
    }

    public Day getDayByDayOfWeek(int dayOfWeek) {
        Calendar c = Calendar.getInstance();
        for (Day d : days) {
            c.setTimeInMillis(d.getDate());
            if (c.get(Calendar.DAY_OF_WEEK) == dayOfWeek) {
                return d;
            }
        }

        return null;
    }

    public int getRemainigHoursForTask(ScheduleTask task) {
        int total = task.getHours();
        for (Day d : days) {
            total -= d.getAssignedHours(task);
        }
        return total;
    }

    public Map<ScheduleTask, Integer> getRemainigHours() {
        Map<ScheduleTask, Integer> remainingHours = new HashMap<ScheduleTask, Integer>();
        for (Map.Entry<ScheduleTask, Integer> e : days.get(0).getAssignedHours().entrySet()) {
            remainingHours.put(e.getKey(), getRemainigHoursForTask(e.getKey()));
        }
        return remainingHours;
    }

}
