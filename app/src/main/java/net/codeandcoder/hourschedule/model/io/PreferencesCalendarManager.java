package net.codeandcoder.hourschedule.model.io;

import android.content.SharedPreferences;

import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.model.calendar.Year;
import net.codeandcoder.hourschedule.model.utils.JSONStructureParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class PreferencesCalendarManager implements CalendarManager {

    private static final String CALENDAR_KEY = "calendar";
    private SharedPreferences prefs;

    public PreferencesCalendarManager(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    @Override
    public Year readData(List<ScheduleTask> tasks) {
        Year data = null;

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        String json = prefs.getString(CALENDAR_KEY + "-" + year, null);

        // If there's no data stored, it generates blank data.
        if (json == null) {
            data = Year.generateBlankYear(year, tasks);
        } else {
            try {
                JSONObject jsonObject = new JSONObject(json);
                data = JSONStructureParser.parseCalendar(jsonObject, tasks);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return data;
    }

    @Override
    public void saveData(Year year) {
        JSONObject jsonToSave = JSONStructureParser.formatYear(year);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(CALENDAR_KEY + "-" + year.getDateYear(), jsonToSave.toString());
        prefsEditor.commit();
    }

}
