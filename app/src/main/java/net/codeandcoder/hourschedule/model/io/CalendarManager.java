package net.codeandcoder.hourschedule.model.io;

import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.model.calendar.Year;

import java.util.List;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public interface CalendarManager {

    public abstract Year readData(List<ScheduleTask> tasks);
    public abstract void saveData(Year year);

}
