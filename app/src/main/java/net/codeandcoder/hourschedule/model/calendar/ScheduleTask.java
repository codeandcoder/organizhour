package net.codeandcoder.hourschedule.model.calendar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class ScheduleTask implements Serializable {

    private String name;
    private int hours;

    public ScheduleTask(String name, int hours) {
        this.name = name;
        this.hours = hours;
    }

    public String getName() {
        return name;
    }

    public int getHours() {
        return hours;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScheduleTask that = (ScheduleTask) o;

        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public static List<ScheduleTask> generateTestTasks() {
        List<ScheduleTask> tasks = new ArrayList<ScheduleTask>();

        tasks.add(new ScheduleTask("testTask1", 15));
        tasks.add(new ScheduleTask("testTask2", 20));
        tasks.add(new ScheduleTask("testTask3", 25));

        return tasks;
    }

}
