package net.codeandcoder.hourschedule.model.io;

import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;

import java.util.List;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public interface TasksManager {

    public abstract List<ScheduleTask> readData();
    public abstract void saveData(List<ScheduleTask> tasks);

}
