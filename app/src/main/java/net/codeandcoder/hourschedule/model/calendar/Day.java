package net.codeandcoder.hourschedule.model.calendar;

import java.util.Map;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class Day {

    private long date;
    private Map<ScheduleTask, Integer> assignedHours;

    public Day(long date, Map<ScheduleTask, Integer> assignedHours) {
        this.date = date;
        this.assignedHours = assignedHours;
    }

    public int getAssignedHours(ScheduleTask task) {
        return assignedHours.get(task);
    }

    public void addTask(ScheduleTask task) {
        assignedHours.put(task, 0);
    }

    public void removeTask(ScheduleTask task) {
        assignedHours.remove(task);
    }

    public long getDate() {
        return date;
    }

    public Map<ScheduleTask, Integer> getAssignedHours() {
        return assignedHours;
    }

    public void takeHours(ScheduleTask task, int hours) {
        assignedHours.put(task, assignedHours.get(task) + hours);
    }

}
