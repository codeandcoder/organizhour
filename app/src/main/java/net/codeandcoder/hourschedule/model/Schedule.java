package net.codeandcoder.hourschedule.model;

import net.codeandcoder.hourschedule.model.calendar.Day;
import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.model.calendar.Week;
import net.codeandcoder.hourschedule.model.calendar.Year;
import net.codeandcoder.hourschedule.model.io.CalendarManager;
import net.codeandcoder.hourschedule.model.io.TasksManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class Schedule {

    private TasksManager tasksManager;
    private CalendarManager calendarManager;

    private Year data;
    private List<ScheduleTask> tasks;

    public Schedule(TasksManager tasksManager, CalendarManager calendarManager) {
        this.tasksManager = tasksManager;
        this.calendarManager = calendarManager;
        init();
    }

    private void init() {
        tasks = tasksManager.readData();
        data = calendarManager.readData(tasks);
    }

    public Week getWeek(long date) {
        return data.getWeek(date);
    }

    public Week getCurrentWeek() {
        return getWeek(Calendar.getInstance().getTimeInMillis());
    }

    public void save() {
        // Add new tasks to all days before saving (and remove old ones)
        for (Week w : data.getWeeks()) {
            for (Day d : w.getDays()) {
                // Remove unused tasks
                List<ScheduleTask> tasksToRemove = new ArrayList<ScheduleTask>();
                for (Map.Entry<ScheduleTask, Integer> e : d.getAssignedHours().entrySet()) {
                    if (!tasks.contains(e.getKey())) {
                        tasksToRemove.add(e.getKey());
                    }
                }
                for (ScheduleTask t : tasksToRemove) {
                    d.removeTask(t);
                }
                // Add new tasks
                for (ScheduleTask task : tasks) {
                    if (!d.getAssignedHours().containsKey(task)) {
                        d.addTask(task);
                    }
                }
            }
        }
        tasksManager.saveData(tasks);
        calendarManager.saveData(data);
    }

    public void addTask(ScheduleTask task) {
        tasks.add(task);
    }

    public void removeTask(ScheduleTask task) {
        tasks.remove(task);
    }

    public List<ScheduleTask> getTasks() {
        return tasks;
    }

    public ScheduleTask getTask(String name) {
        for (ScheduleTask task : tasks) {
            if (task.getName().equalsIgnoreCase(name)) {
                return  task;
            }
        }

        return null;
    }

}
