package net.codeandcoder.hourschedule.model.calendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class Year {

    private List<Week> weeks;

    public Year(List<Week> weeks) {
        this.weeks = weeks;
    }

    public List<Week> getWeeks() {
        return weeks;
    }

    public Week getWeek(long date) {
        for (Week w : weeks) {
            Day day1 = w.getDay(0);
            Day day2 = w.getDay(6);
            if (date >= day1.getDate() && date < day2.getDate() + 86400000) {
                return w;
            }
        }

        return null;
    }

    public Day getDay(long date) {
        Day day = null;

        while (day != null) {
            for (int i = 0; i < weeks.size() && day != null; i++) {
                day = weeks.get(i).getDayByDate(date);
            }
        }

        return day;
    }

    public int getDateYear() {
        Week lastWeek = weeks.get(weeks.size()-1);
        List<Day> lastWeekDays = lastWeek.getDays();
        Day lastDay = lastWeekDays.get(lastWeekDays.size()-1);
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(lastDay.getDate());
        return c.get(Calendar.YEAR);
    }

    public static Year generateBlankYear(int year, List<ScheduleTask> tasks) {
        Year blankYear = null;

        try {
            String formattedYear = format4DigitsYear(year);
            String dateString = formattedYear + "-01-01";
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(formatter.parse(dateString));
            c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

            List<Week> weeks = new ArrayList<Week>();
            List<Day> days = new ArrayList<Day>();

            for (int i = 1; i <= 364; i++) {
                Map<ScheduleTask, Integer> assignedHours = new HashMap<ScheduleTask, Integer>();
                for (int j = 0; j < tasks.size(); j++) {
                    assignedHours.put(tasks.get(j), 0);
                }
                c.add(Calendar.DAY_OF_YEAR, 1);
                days.add(new Day(c.getTimeInMillis(), assignedHours));

                if (i % 7 == 0) {
                    weeks.add(new Week(days));
                    days = new ArrayList<Day>();
                }
            }

            blankYear = new Year(weeks);
        } catch (ParseException ex){
            ex.printStackTrace();
        }

        return blankYear;
    }

    private static String format4DigitsYear(int year) {
        String formattedYear = String.valueOf(year);
        while (formattedYear.length() < 4) {
            formattedYear = "0" + formattedYear;
        }
        return formattedYear;
    }

}
