package net.codeandcoder.hourschedule.model.utils;

import net.codeandcoder.hourschedule.model.calendar.Day;
import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.model.calendar.Week;
import net.codeandcoder.hourschedule.model.calendar.Year;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class JSONStructureParser {

    private static class Task {

        /*
            {
                tasks : [
                    {
                        name : "task1",
                        hours : 15
                    }
                ]
            }
         */

        public static final String TASKS = "tasks";
        public static final String NAME = "name";
        public static final String HOURS = "hours";
    }

    private static class TaskCalendar {

        /*
            {
                weeks : [
                    {
                        days : [
                            {
                                date : 1231566123153,
                                assigned_hours : [
                                    {
                                        task : "task1",
                                        hours : 2
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
         */

        public static final String WEEKS = "weeks";
        public static final String DAYS = "days";
        public static final String DATE = "date";
        public static final String ASSINGED_HOURS = "assigned_hours";
        public static final String TASK = "task";
        public static final String HOURS = "hours";
    }

    public static List<ScheduleTask> parseTasks(JSONObject tasks) {
        List<ScheduleTask> finalList = new ArrayList<ScheduleTask>();

        try {
            JSONArray tasksList = tasks.getJSONArray(Task.TASKS);
            for (int i = 0; i < tasksList.length(); i++) {
                JSONObject task = tasksList.getJSONObject(i);
                String name = task.getString(Task.NAME);
                int hours = task.getInt(Task.HOURS);
                finalList.add(new ScheduleTask(name,hours));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return finalList;
    }

    public static Year parseCalendar(JSONObject calendar, List<ScheduleTask> tasks) {
        Year year = null;

        Map<String, ScheduleTask> tasksMap = tasksToMap(tasks);

        try {
            List<Week> weeksList = new ArrayList<Week>();
            JSONArray weeks = calendar.getJSONArray(TaskCalendar.WEEKS);
            for (int i = 0; i < weeks.length(); i++) {
                JSONObject week = weeks.getJSONObject(i);
                List<Day> daysList = new ArrayList<Day>();
                JSONArray days = week.getJSONArray(TaskCalendar.DAYS);
                for (int j = 0; j < days.length(); j++) {
                    JSONObject day = days.getJSONObject(j);
                    Map<ScheduleTask, Integer> assignedHours = new HashMap<ScheduleTask, Integer>();
                    long date = day.getLong(TaskCalendar.DATE);
                    JSONArray hours = day.getJSONArray(TaskCalendar.ASSINGED_HOURS);
                    for (int l = 0; l < hours.length(); l++) {
                        JSONObject hour = hours.getJSONObject(l);
                        String taskName = hour.getString(TaskCalendar.TASK);
                        int taskHours = hour.getInt(TaskCalendar.HOURS);
                        assignedHours.put(tasksMap.get(taskName), taskHours);
                    }
                    daysList.add(new Day(date, assignedHours));
                }
                weeksList.add(new Week(daysList));
            }
            year = new Year(weeksList);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return year;
    }

    private static Map<String, ScheduleTask> tasksToMap(List<ScheduleTask> tasks) {
        Map<String, ScheduleTask> tasksMap = new HashMap<String, ScheduleTask>();
        for (ScheduleTask t : tasks) {
            tasksMap.put(t.getName(), t);
        }
        return tasksMap;
    }

    public static JSONObject formatYear(Year year) {
        JSONObject data = new JSONObject();
        try {
            JSONArray weeks = new JSONArray();
            List<Week> weeksList = year.getWeeks();

            for (Week w : weeksList) {
                JSONObject week = new JSONObject();

                JSONArray days = new JSONArray();
                List<Day> daysList = w.getDays();

                for (Day d : daysList) {
                    JSONObject day = new JSONObject();
                    day.put(TaskCalendar.DATE, d.getDate());

                    JSONArray hours = new JSONArray();
                    Map<ScheduleTask, Integer> assignedHours = d.getAssignedHours();

                    for (Map.Entry<ScheduleTask, Integer> e : assignedHours.entrySet()) {
                        JSONObject hour = new JSONObject();
                        hour.put(TaskCalendar.TASK, e.getKey().getName());
                        hour.put(TaskCalendar.HOURS, e.getValue());
                        hours.put(hour);
                    }

                    day.put(TaskCalendar.ASSINGED_HOURS, hours);
                    days.put(day);
                }

                week.put(TaskCalendar.DAYS, days);
                weeks.put(week);
            }

            data.put(TaskCalendar.WEEKS, weeks);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return data;
    }

    public static JSONObject formatTasks(List<ScheduleTask> tasksList) {
        JSONObject data = new JSONObject();

        try {
            JSONArray tasks = new JSONArray();

            for (ScheduleTask t : tasksList) {
                JSONObject task = new JSONObject();
                task.put(Task.NAME, t.getName());
                task.put(Task.HOURS, t.getHours());
                tasks.put(task);
            }

            data.put(Task.TASKS, tasks);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return data;
    }

}
