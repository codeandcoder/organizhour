package net.codeandcoder.hourschedule.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import net.codeandcoder.hourschedule.model.Schedule;
import net.codeandcoder.hourschedule.model.io.CalendarManager;
import net.codeandcoder.hourschedule.model.io.PreferencesCalendarManager;
import net.codeandcoder.hourschedule.model.io.PreferencesTasksManager;
import net.codeandcoder.hourschedule.model.io.TasksManager;

/**
 * Created by Santi Ruiz on 03/03/2015.
 */
public class ScheduleController {

    private static final String SP_FILE = "schedule-data";
    private static ScheduleController singleton;

    public static ScheduleController getInstance(Activity activity) {
        if (singleton == null) {
            SharedPreferences sp = activity.getSharedPreferences(SP_FILE, Context.MODE_PRIVATE);
            TasksManager tasksManager = new PreferencesTasksManager(sp);
            CalendarManager calendarManager = new PreferencesCalendarManager(sp);
            singleton = new ScheduleController(tasksManager, calendarManager);
        }

        return singleton;
    }

    private Schedule schedule;

    private ScheduleController(TasksManager tasksManager, CalendarManager calendarManager) {
        schedule = new Schedule(tasksManager, calendarManager);
    }

    public Schedule getSchedule() {
        return schedule;
    }

}
