package net.codeandcoder.hourschedule.view.screens;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import net.codeandcoder.hourschedule.R;
import net.codeandcoder.hourschedule.controller.ScheduleController;
import net.codeandcoder.hourschedule.model.Schedule;
import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.view.NavigationActivity;
import net.codeandcoder.hourschedule.view.adapters.TasksAdapter;

import java.util.List;

/**
 * Created by Santi Ruiz on 04/03/2015.
 */
public class HistoryScreen extends Screen {

    private Schedule schedule;

    public HistoryScreen (NavigationActivity navigation) {
        super(navigation);
        schedule = ScheduleController.getInstance(navigation).getSchedule();
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_history, container, false);

        return rootView;
    }

}
