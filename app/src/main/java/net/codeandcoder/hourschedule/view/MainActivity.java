package net.codeandcoder.hourschedule.view;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import net.codeandcoder.hourschedule.R;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView touchText = (ImageView) findViewById(R.id.touchText);
        Animation touchTextAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.touch);
        touchText.startAnimation(touchTextAnimation);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        Intent intent = new Intent(this, NavigationActivity.class);
        startActivity(intent);

        return super.onTouchEvent(event);
    }

}
