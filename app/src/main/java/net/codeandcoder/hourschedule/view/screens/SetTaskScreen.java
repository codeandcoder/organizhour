package net.codeandcoder.hourschedule.view.screens;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import net.codeandcoder.hourschedule.R;
import net.codeandcoder.hourschedule.controller.ScheduleController;
import net.codeandcoder.hourschedule.model.Schedule;
import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.view.NavigationActivity;

/**
 * Created by Santi Ruiz on 05/03/2015.
 */
public class SetTaskScreen extends Screen implements AdapterView.OnClickListener {

    private Schedule schedule;
    private ScheduleTask task;

    private EditText taskName;
    private EditText taskHours;

    private ImageButton goBack;
    private Button save;

    public SetTaskScreen (NavigationActivity navigation, ScheduleTask task) {
        super(navigation);
        schedule = ScheduleController.getInstance(navigation).getSchedule();
        this.task = task;
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_set_task, container, false);

        taskName = (EditText) rootView.findViewById(R.id.task_name);
        taskHours = (EditText) rootView.findViewById(R.id.task_max_hours);

        goBack = (ImageButton) rootView.findViewById(R.id.go_back);
        goBack.setOnClickListener(this);
        save = (Button) rootView.findViewById(R.id.save);
        save.setOnClickListener(this);

        task = task == null ? new ScheduleTask("New Task", 15) : task;
        taskName.setText(task.getName());
        taskHours.setText(String.valueOf(task.getHours()));

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == goBack.getId()) {
            navigation.setScreen(2);
        } else if (v.getId() == save.getId()) {
            task.setName(taskName.getText().toString());
            task.setHours(Integer.parseInt(taskHours.getText().toString()));
            if (!schedule.getTasks().contains(task)) {
                schedule.addTask(task);
            }
            schedule.save();
            navigation.setScreen(2);
        }
    }

}
