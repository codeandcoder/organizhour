package net.codeandcoder.hourschedule.view.adapters;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.codeandcoder.hourschedule.R;
import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;

import java.util.List;

/**
 * Created by Santi Ruiz on 04/03/2015.
 */
public class TasksAdapter extends BaseAdapter {

    private List<ScheduleTask> tasks;
    private LayoutInflater li;
    private Resources resources;

    public TasksAdapter(LayoutInflater li, List<ScheduleTask> tasks, Resources res) {
        this.li = li;
        this.tasks = tasks;
        this.resources = res;
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Object getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        ScheduleTask item = tasks.get(position);

        if (convertView == null) {
            convertView = li.inflate(R.layout.task_item_layout, parent, false);
            vh = new ViewHolder(convertView);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        vh.taskName.setText(item.getName());

        return convertView;
    }

    private static class ViewHolder {

        View wholeView;
        TextView taskName;

        public ViewHolder(View view) {
            this.wholeView = view;
            this.taskName = (TextView) view.findViewById(R.id.task_name);
        }
    }

}
