package net.codeandcoder.hourschedule.view.screens;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import net.codeandcoder.hourschedule.R;
import net.codeandcoder.hourschedule.controller.ScheduleController;
import net.codeandcoder.hourschedule.model.Schedule;
import net.codeandcoder.hourschedule.model.calendar.Day;
import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.view.NavigationActivity;
import net.codeandcoder.hourschedule.view.adapters.RemainingHoursAdapter;
import net.codeandcoder.hourschedule.view.adapters.TasksAdapter;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by Santi Ruiz on 04/03/2015.
 */
public class TaskManagementScreen extends Screen implements AdapterView.OnItemClickListener,
        View.OnClickListener, AdapterView.OnItemLongClickListener {

    private Schedule schedule;

    private TasksAdapter tasksAdapter;
    private ListView taskList;

    private Button create;

    public TaskManagementScreen (NavigationActivity navigation) {
        super(navigation);
        schedule = ScheduleController.getInstance(navigation).getSchedule();
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_task_management, container, false);

        create = (Button) rootView.findViewById(R.id.create);
        create.setOnClickListener(this);

        taskList = (ListView) rootView.findViewById(R.id.list_view);
        List<ScheduleTask> tasks = schedule.getTasks();
        tasksAdapter = new TasksAdapter(navigation.getLayoutInflater(), tasks, navigation.getResources());
        taskList.setAdapter(tasksAdapter);
        taskList.setOnItemClickListener(this);
        taskList.setOnItemLongClickListener(this);

        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        navigation.setScreen(3, tasksAdapter.getItem(position));
    }

    @Override
    public void onClick(View v) {
        navigation.setScreen(3);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        schedule.removeTask((ScheduleTask)tasksAdapter.getItem(position));

        List<ScheduleTask> tasks = schedule.getTasks();
        tasksAdapter = new TasksAdapter(navigation.getLayoutInflater(), tasks, navigation.getResources());
        taskList.setAdapter(tasksAdapter);

        schedule.save();

        return false;
    }
}
