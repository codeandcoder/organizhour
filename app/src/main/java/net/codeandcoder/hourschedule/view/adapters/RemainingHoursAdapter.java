package net.codeandcoder.hourschedule.view.adapters;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.codeandcoder.hourschedule.R;
import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Santi Ruiz on 04/03/2015.
 */
public class RemainingHoursAdapter extends BaseAdapter {

    private List<TaskItem> remainingHours;
    private LayoutInflater li;
    private Resources resources;

    public RemainingHoursAdapter(LayoutInflater li, Map<ScheduleTask, Integer> remainingHours, Resources res) {
        this.li = li;
        this.remainingHours = generateTaskItems(remainingHours);
        this.resources = res;
    }

    @Override
    public int getCount() {
        return remainingHours.size();
    }

    @Override
    public Object getItem(int position) {
        return remainingHours.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        TaskItem item = remainingHours.get(position);

        if (convertView == null) {
            convertView = li.inflate(R.layout.rh_item_layout, parent, false);
            vh = new ViewHolder(convertView);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        vh.taskName.setText(item.task.getName());
        vh.hours.setText(String.valueOf(item.hours));

        return convertView;
    }

    private List<TaskItem> generateTaskItems(Map<ScheduleTask, Integer> remainingHours) {
        List<TaskItem> items = new ArrayList<>();

        for (Map.Entry<ScheduleTask, Integer> rh : remainingHours.entrySet()) {
            TaskItem item = new TaskItem();
            item.task = rh.getKey();
            item.hours = rh.getValue();
            items.add(item);
        }

        return items;
    }

    private static class ViewHolder {

        View wholeView;
        TextView taskName;
        TextView hours;

        public ViewHolder(View view) {
            this.wholeView = view;
            this.taskName = (TextView) view.findViewById(R.id.task_name);
            this.hours = (TextView) view.findViewById(R.id.hours);
        }
    }

    public static class TaskItem {

        public ScheduleTask task;
        public int hours;

    }

}
