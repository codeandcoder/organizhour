package net.codeandcoder.hourschedule.view.screens;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import net.codeandcoder.hourschedule.R;
import net.codeandcoder.hourschedule.controller.ScheduleController;
import net.codeandcoder.hourschedule.model.Schedule;
import net.codeandcoder.hourschedule.model.calendar.Day;
import net.codeandcoder.hourschedule.model.calendar.ScheduleTask;
import net.codeandcoder.hourschedule.view.NavigationActivity;
import net.codeandcoder.hourschedule.view.adapters.RemainingHoursAdapter;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by Santi Ruiz on 04/03/2015.
 */
public class RemainingHoursScreen extends Screen implements AdapterView.OnItemClickListener {

    private Schedule schedule;
    private ListView taskList;
    private RemainingHoursAdapter remainingHoursAdapter;

    public RemainingHoursScreen (NavigationActivity navigation) {
        super(navigation);
        schedule = ScheduleController.getInstance(navigation).getSchedule();
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_remaining_hours, container, false);

        taskList = (ListView) rootView.findViewById(R.id.list_view);
        Map<ScheduleTask, Integer> remainingHours = schedule.getCurrentWeek().getRemainigHours();
        remainingHoursAdapter = new RemainingHoursAdapter(navigation.getLayoutInflater(),
                remainingHours, navigation.getResources());
        taskList.setAdapter(remainingHoursAdapter);
        taskList.setOnItemClickListener(this);

        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Calendar c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        Day day = schedule.getCurrentWeek().getDayByDayOfWeek(dayOfWeek);
        RemainingHoursAdapter.TaskItem item = (RemainingHoursAdapter.TaskItem) remainingHoursAdapter.getItem(position);
        if (item.hours > 0) {
            day.takeHours(item.task, 1);
        }

        // Reload list
        Map<ScheduleTask, Integer> remainingHours = schedule.getCurrentWeek().getRemainigHours();
        remainingHoursAdapter = new RemainingHoursAdapter(navigation.getLayoutInflater(),
                remainingHours, navigation.getResources());
        taskList.setAdapter(remainingHoursAdapter);
    }

}
