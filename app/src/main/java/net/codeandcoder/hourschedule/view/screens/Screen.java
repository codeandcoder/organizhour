package net.codeandcoder.hourschedule.view.screens;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.codeandcoder.hourschedule.R;
import net.codeandcoder.hourschedule.view.NavigationActivity;

/**
 * Created by Santi Ruiz on 04/03/2015.
 */
public abstract class Screen {

    protected NavigationActivity navigation;
    protected View rootView;

    public Screen(NavigationActivity navigation) {
        this.navigation = navigation;
    }

    protected abstract View initView(LayoutInflater inflater, ViewGroup container);

    public View getRootView() {
        return rootView;
    }

}
